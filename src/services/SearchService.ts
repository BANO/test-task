import { inject, injectable } from 'inversify'
import { action, computed, makeAutoObservable } from 'mobx'

import { Charter } from '../types/external-api'
import { ChartersService } from './ChartersService'

@injectable()
export default class SearchService {
    public searchEpisode: number | null = null
    @inject(ChartersService)
    private chartersService!: ChartersService

    constructor() {
        makeAutoObservable(this)
    }

    @computed
    public get items(): Charter[] {
        const charters = this.chartersService.charters
        if (this.searchEpisode === null) return charters

        return charters.filter(filterFabric(this.searchEpisode))
    }

    @action
    async lookup(episode: null | number): Promise<Charter[]> {
        await this.chartersService.getCharters()

        this.searchEpisode = episode

        return this.items
    }
}

function filterFabric(neededEpisode: number) {
    return (charter: Charter) =>
        charter.episode.some((episodeUrl) => {
            const lastPathPart = episodeUrl.split('/').slice(-1)[0]
            return lastPathPart === neededEpisode.toString()
        })
}
