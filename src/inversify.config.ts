import { Container } from 'inversify'

import { ChartersService } from './services/ChartersService'
import SearchService from './services/SearchService'

export const container = new Container()
container.bind<ChartersService>(ChartersService).toSelf().inSingletonScope()
container.bind<SearchService>(SearchService).toSelf().inSingletonScope()
