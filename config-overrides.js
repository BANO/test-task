const {
    override,
    addDecoratorsLegacy,
    disableEsLint,
    addBabelPlugin
} = require('customize-cra')

module.exports = {
    webpack: override(
        disableEsLint(),
        addDecoratorsLegacy(),
        process.env.NODE_ENV === 'development' && addBabelPlugin('react-hot-loader/babel')
    )
}
